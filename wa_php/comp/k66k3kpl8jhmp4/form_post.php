<?php
include('../../../wa_php/waCommon.php');
$reply_to="";
$mail_content="";
$res=true;
$success_message="";
$message_error="";
$form_values_array = array();
$reply_to = '';
$lng = waRetrievePostParameter('lng');
$message_error_recaptcha = waRetrievePostParameter('message_error_recaptcha');
if (PHP_VERSION_ID < 50207) 
{
{
    $message_error='Error -> Minimal PHP version is 5.2.7 ! , your version is '.phpversion();
    echo waFormatResultForm(false,$message_error,'');
    exit;
}
}
array_push($form_values_array, waRetrievePostParameter('firstname_field_0'));
array_push($form_values_array, waRetrievePostParameter('lastname_field_1'));
array_push($form_values_array, waRetrievePostParameter('mail_field_2'));
array_push($form_values_array, waRetrievePostParameter('phone_field_3'));
array_push($form_values_array, waRetrievePostParameter('field_4'));
array_push($form_values_array, waRetrievePostParameter('field_5'));
?>
<?php
$g_captcha_response = waRetrievePostParameter('g-recaptcha-response');
$data = array('secret' => "6LciZtUUAAAAAPLqj_r6EIJ-HfA3XRYP7tIfoqs9",'response' => $g_captcha_response);
$postString = http_build_query($data, '', '&');
$opts = array('http' => array('method'  => 'POST','header'  => 'Content-type: application/x-www-form-urlencoded','content' => $postString));
$context = stream_context_create($opts);
$result_json = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
if ($result_json ===FALSE)
{
      $result_json = getCurlData("https://www.google.com/recaptcha/api/siteverify?".$postString);
}
$response_google = json_decode ($result_json,true);
if ($response_google['success']==false)
{
    $message_error=$message_error_recaptcha;
    echo waFormatResultForm(false,$message_error,'');
    exit;
}
?>
<?php
$item_label = '';
if ($lng == "pt")
$item_label = "Digite seu primeiro nome"."\n";
$mail_content .= $item_label;
$mail_content .= $form_values_array[0]."\n";
$mail_content .= "\n";
$item_label = '';
if ($lng == "pt")
$item_label = "Digite seu sobrenome"."\n";
$mail_content .= $item_label;
$mail_content .= $form_values_array[1]."\n";
$mail_content .= "\n";
$item_label = '';
if ($lng == "pt")
$item_label = "Digite seu e-mail"."\n";
$mail_content .= $item_label;
$mail_content .= $form_values_array[2]."\n";
$mail_content .= "\n";
$reply_to=$form_values_array[2];
$item_label = '';
if ($lng == "pt")
$item_label = "Digite seu contato ou whatsapp"."\n";
$mail_content .= $item_label;
$mail_content .= $form_values_array[3]."\n";
$mail_content .= "\n";
$item_label = '';
if ($lng == "pt")
$item_label = "Escolha a opção desejada"."\n";
$mail_content .= $item_label;
$mail_content .= $form_values_array[4]."\n";
$mail_content .= "\n";
$item_label = '';
if ($lng == "pt")
$item_label = "Digite a sua mensagem"."\n";
$mail_content .= $item_label;
$mail_content .= $form_values_array[5]."\n";
$mail_content .= "\n";
$destinataire="progeplan@progeplan.eng.br";
$res = waSendMail($destinataire,"Formulario de Contato Site",$mail_content,$reply_to);
$message_error=waGetError();
?>
<?php
if (($res==true) && ($waErrorPhpMailReporting==1)) $message_error="";
echo waFormatResultForm($res,$message_error,'');
?>
